# The Branded Root


[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)]()

The Branded Root is a demo online book store which has following features.

##### Admin Panel
1. Create new categories 
2. Add books (including adding to a category) 
3. Search for a book by title and/or author 
4. View individual books details   (advanced version will allow viewing visitor statistics) 

##### Consumer part
1. Browse books by categories
2. View all books in a category (an advanced version will perform pagination) 
3. Add a book to a shopping basket
4. View the shopping basket  (advanced version will allow editing of shopping basket) 



#
#
#
#
#

#####     *** THIS IS AN ASSIGNMENT DONE FOR ADVANCED SERVER SIDE MODULE***
