<html>
<head>

    <title>Add Books</title>
    <?php include 'components/header.php' ?>
    <?php include 'components/navbar.php' ?>

</head>
<body>
<div style='padding:60px;'>
    <h2> Add a new Category </h2>
    <br>
    <div class="row">

        <?php echo form_open('admin/addCategory'); ?>
        <div class="col">

            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-default">Category</span>
                </div>
                <input name="category" type="text" class="form-control" aria-label="Sizing example input"
                       aria-describedby="inputGroup-sizing-default">
                <span class="text-danger"> <?php echo form_error('category') ?></span>
            </div>
            <div class="col">
                <button type="submit" class="btn btn-success">Add Category</button>
            </div>
        </div>
    </div>
</div>
</body>
