<!DOCTYPE html>
<html>
<head>

    <title>Add Books</title>
    <?php include 'components/header.php' ?>
    <?php include 'components/navbar.php' ?>

</head>
<body>
<div style='padding:60px;'>
    <h2> Add a book </h2>
    <br>
    <div class="row">

        <?php echo form_open('admin/addBooks'); ?>
        <div class="col">

            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-default">Title</span>
                </div>
                <input name="title" type="text" class="form-control" aria-label="Sizing example input"
                       aria-describedby="inputGroup-sizing-default">
                <span class="text-danger"> <?php echo form_error('title') ?></span>
            </div>

            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-default">Author</span>
                </div>
                <input name="author" type="text" class="form-control" aria-label="Sizing example input"
                       aria-describedby="inputGroup-sizing-default">
                <span class="text-danger"> <?php echo form_error('author') ?></span>
            </div>

            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-default">ISBN</span>
                </div>
                <input name="isbn" type="text" class="form-control" aria-label="Sizing example input"
                       aria-describedby="inputGroup-sizing-default">
                <span class="text-danger"> <?php echo form_error('isbn') ?></span>

            </div>

            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-default">Edition</span>
                </div>
                <input name="edition" type="text" class="form-control" aria-label="Sizing example input"
                       aria-describedby="inputGroup-sizing-default">
                <span class="text-danger"> <?php echo form_error('edition') ?></span>

            </div>

            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-default">Price $</span>
                </div>
                <input name="price" type="text" class="form-control" aria-label="Sizing example input"
                       aria-describedby="inputGroup-sizing-default">
                <span class="text-danger"> <?php echo form_error('price') ?></span>

            </div>

            <div class="form-group mb-3">
                <select name="category_id" id="inputState" class="form-control">
                    <option value=" " selected="selected">Choose Category...</option>
                    <?php foreach ($category as $category_data): ?>
                        <option value="<?php echo $category_data['id']; ?>"><?php echo $category_data['name']; ?></option>
                    <?php endforeach; ?>
                </select>
                <span class="text-danger"> <?php echo form_error('category_id') ?></span>
            </div>

        </div>

        <div class="col">

            <div class="form-group">
                <label for="exampleFormControlFile1">Example file input</label>
                <input type="file" class="form-control-file" id="exampleFormControlFile1">
            </div>

        </div>
    </
    >

</div>
<div class="row" style="padding-top: 50px">
    <div class="col">

    </div>
    <div class="col">
        <button type="submit" class="btn btn-success">Add Book</button>
    </div>


</div>


</body>
</html>