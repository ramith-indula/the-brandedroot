<!DOCTYPE html>
<html>
<head>

    <?php include 'components/header.php' ?>

    <title>Admin Panel</title>
    <link href="application\views\admin\components\signin.css" rel="stylesheet">


</head>


<body class="text-center" data-gr-c-s-loaded="true">

<form method="post" class="form-signin" action="<?php echo base_url() ?>admin/login">
    <img class="mb-4" src="application\views\components\BRnew1.png" alt="" width="72" height="72">
    <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>

    <label for="userName" class="sr-only">User Name</label>
    <input type="text" name="userName" class="form-control" placeholder="User Name" autofocus="">
    <span class="text-danger"> <?php echo form_error('userName') ?></span>

    <label for="password" class="sr-only">Password</label>
    <input type="password" name="password" class="form-control" placeholder="Password">
    <span class="text-danger"> <?php echo form_error('password') ?></span>

    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    <p class="mt-5 mb-3 text-muted">© 2018-2019</p>
</form>

</body>
</html>
