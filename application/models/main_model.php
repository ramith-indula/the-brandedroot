<?php
defined('BASEPATH') or exit('No direct script access allowed');

class main_model extends CI_Model
{

    public function adminLogin($username, $password)
    {
        $this->db->where('username', $username);
        $this->db->where('password', $password);

        $result = $this->db->get('admin_login');

        if ($result->num_rows() == 1) {
            return $result->row(0)->id;
        } else {
            return false;
        }
    }


    public function create_book()
    {

        $data = array(
            'title' => $this->input->post('title'),
            'author' => $this->input->post('author'),
            'isbn' => $this->input->post('isbn'),
            'edition' => $this->input->post('edition'),
            'price' => $this->input->post('price'),
            'category_id' => $this->input->post('category_id')

        );

        return $this->db->insert('books', $data);
    }

    public function create_category()
    {
        $data = array(
            'name' => $this->input->post('category')
        );

        return $this->db->insert('category', $data);
    }

    public function get_category()
    {
        $this->db->order_by('name');
        $query = $this->db->get('category');
        return $query->result_array();


    }
}
