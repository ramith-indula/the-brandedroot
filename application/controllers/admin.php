<?php
defined('BASEPATH') or exit('No direct script access allowed');

class admin extends CI_Controller
{


    public function index()
    {

        $data['title'] = 'Admin Panel';

        $this->load->view('admin/adminpanel', $data);


    }


    public function view($page = 'home')
    {
        if (!file_exists(APPPATH . 'views/admin/' . $page . '.php')) {
            show_404();
        }
        $data['title'] = $page;
        $data['category'] = $this->main_model->get_category();


        $this->load->view('admin/' . $page, $data);

    }


//Admin Panel login form validation
    public function login()
    {

        $this->form_validation->set_rules("userName", "User Name", 'required');
        $this->form_validation->set_rules("password", "Password", 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('admin/adminpanel');

        } else {
            $username = $this->input->post('userName');
            $password = $this->input->post('password');

            $this->load->model('main_model');
            $user_id = $this->main_model->adminLogin($username, $password);

            if ($user_id) {

                $this->load->view('admin/adminHome');

            } else {

                //this is temp.. add proper error handling
                redirect('Welcome/adminPanel');

            }


        }
    }


    public function addBooks()
    {

        $data['category'] = $this->main_model->get_category();

        $this->form_validation->set_rules("title", "Title", 'required');
        $this->form_validation->set_rules("author", "Author", 'required');
        $this->form_validation->set_rules("isbn", "ISBN", 'required');
        $this->form_validation->set_rules("edition", "Edition", 'required');
        $this->form_validation->set_rules("price", "Price", 'required');


        $this->form_validation->set_rules("category_id", "Category", 'required');

        if ($this->form_validation->run() == false) {

            $this->load->view('admin/addBooks', $data);

        } else {

            $this->main_model->create_book();
            redirect('admin/addBooks');

        }


    }

    public function addCategory()
    {
        $this->form_validation->set_rules("category", "Category", 'required');

        if ($this->form_validation->run() == false) {
            $this->load->view('admin/addCategory');

        } else {

            $this->main_model->create_category();
            redirect('admin/addCategory');
        }

    }

}
